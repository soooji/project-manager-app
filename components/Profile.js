import React, { Component } from 'react';
import { Container, Header, Left, Body, Right, Button, Icon, Title } from 'native-base';
import {Modal, Text, TouchableHighlight, ActivityIndicator, View, AsyncStorage,StyleSheet ,TouchableOpacity,Image,Dimensions} from 'react-native';
import LinearGradient from 'react-native-linear-gradient';
import Spinner from 'react-native-loading-spinner-overlay';
import RF from 'react-native-responsive-fontsize'
import { ScrollView } from 'react-native-gesture-handler';
import axios from 'axios'
import AutoHeightImage from 'react-native-auto-height-image';
const WIN = Dimensions.get('screen');
export default class Login extends Component{
    constructor(props) {
        super(props);
        this.state={
            isLoading:false,
            first_name:'',
            last_name:'',
            factory: '',
            role: '',
            factory_info: {
                logo: {
                    url:''
                }
            },
            instructions: []
        }
    }
    loadUserInfo = async()=> {
        try {
            const first_name = await AsyncStorage.getItem('first_name');
            const last_name = await AsyncStorage.getItem('last_name');
            const factory = await AsyncStorage.getItem('factory');
            this.setState({first_name,last_name,factory})
        } catch (error) {
            alert(error);
            this.props.navigation.navigate('page7')
        }
    }
    getInfo = async()=> {
        try {
            this.setState({isLoading: true});
            const token = await AsyncStorage.getItem('token');
            const username = await AsyncStorage.getItem('username');
            axios.get(`http://dev.pm.coleo.me/api/employees/${username}/${token}/`)
            .then(
                r=>{
                    this.setState({
                        instructions: r.data.user.allowed_instructions,
                        first_name: r.data.user.first_name,
                        last_name: r.data.user.last_name,
                        factory: r.data.user.factory.title,
                        factory_info: r.data.user.factory,
                    })
                    this.setState({isLoading: false});
                }
            )
            .catch(e=>{alert(e);this.setState({isLoading: false});})
        } catch(e) {
            this.setState({isLoading: false});
            this.props.navigation.navigate('page7');
        }
    }
    logout(){
        AsyncStorage.clear();
        this.props.navigation.navigate('page7');
      }
    componentDidMount(){
        this.loadUserInfo();
        this.getInfo();
    }
    stringToHslColor(str, s, l) {
        var hash = 0;
        for (var i = 0; i < str.length; i++) {
          hash = str.charCodeAt(i) + ((hash << 5) - hash);
        }
        var h = hash % 360;
        return 'hsl('+h+', '+s+'%, '+l+'%)';
    }
    render(){
        return(
            <LinearGradient colors={['#698ffa','#678df9','#648af9','#6389f9','#6087f9','#5f86f9','#5b82f8','#567ef8','#527bf7','#4f78f7','#4d76f7','#4b74f7','#4972f6','#4b74f7']} style={styles.modal_style} >
            <Spinner
          visible={this.state.isLoading}
          textContent={'Loading...'}
          textStyle={{color: '#FFF'}}
        />
            <ScrollView style={{paddingTop:10,marginLeft:'auto',marginRight:'auto',width:'100%'}} contentContainerStyle={{paddingBottom:150}}>
                <View style={styles.user_info}>
                    <LinearGradient style={[styles.picture_style,{justifyContent:'center',alignItems:'center'}]} colors={[this.stringToHslColor(this.state.first_name+this.state.last_name,30,80),this.stringToHslColor(this.state.first_name+this.state.last_name,80,40)]} >
                        <Text style={{color:'white',fontFamily:'K2D-Regular',fontSize:40,textAlign:'center',paddingBottom:7}}>{this.state.first_name.slice(0,1)}</Text>
                    </LinearGradient>
                    <View style={styles.user_name}>
                        <Text style={{color:'white' ,fontWeight:'bold' ,fontSize:25}}>
                            {this.state.first_name+" "+this.state.last_name}
                        </Text>
                    </View>
                    <View style={{backgroundColor:'#3d69e6',marginTop:10,padding:7,borderRadius:4}}>
                        <Text style={{color:'white' ,fontSize:18}}>Factory : {this.state.factory}</Text>
                    </View>
                </View>
                <View style={styles.user_work}>
                {!this.state.isLoading ?
                    <View style={{width:'95%',marginLeft:'auto',marginRight:'auto',backgroundColor:'#3d69e6',borderRadius:10,padding:0}}>
                        <View style={{width:'100%',paddingBottom:10,borderBottomColor:'rgba(0,0,0,.2)',paddingTop:10,borderBottomWidth:0}}>
                            <Text style={{textAlign:'center',fontSize:RF(2.7),color:'white',fontFamily:'K2D-Bold'}}>Allowed instructions</Text>
                        </View>
                        <View style={{width:'100%',height:'auto'}}>
                        {
                            this.state.instructions.map(
                                (v,k)=>
                                    <View style={{width:'100%',padding:10,borderTopColor:'rgba(0,0,0,.2)',paddingTop:10,borderTopWidth:1}}>
                                        <Text style={{textAlign:'left',color:'white',fontSize:RF(2.4),fontFamily:'K2D-Regular'}}>
                                        {k+1}. {v.name}
                                        </Text>
                                    </View>    
                                
                            )
                        }
                        {!this.state.isLoading && this.state.instructions.length == 0 ?
                                <Text style={{textAlign:'center',color:'rgba(255,255,255,.7)',fontSize:RF(2.4),fontFamily:'K2D-Regular',paddingBottom:15}}>
                                        Nothing!
                                </Text>
                            : null}
                        </View>
                    </View> : null}

                {!this.state.isLoading ?
                    <View style={{width:'95%',marginLeft:'auto',marginRight:'auto',marginTop:20,backgroundColor:'#3d69e6',borderRadius:10,padding:0}}>
                        <View style={{width:'100%',paddingBottom:10,borderBottomColor:'rgba(0,0,0,.2)',paddingTop:10,borderBottomWidth:0}}>
                            <Text style={{textAlign:'center',fontSize:RF(2.7),color:'white',fontFamily:'K2D-Bold'}}>Factory information</Text>
                        </View>
                            <View style={{width:'100%',height:'auto'}}>
                                <AutoHeightImage width={WIN.width/4} style={{marginTop:10,marginBottom:10,marginLeft:'auto',marginRight:'auto'}} source={{uri:`http://dev.pm.coleo.me${this.state.factory_info.logo.url}`}}/>
                                <Text style={{textAlign:'left',fontSize:RF(2.3),color:'white',padding:15,fontFamily:'K2D-Regular'}}>
                                    {this.state.factory_info.description}
                                </Text>
                            </View>
                    </View>
                    :null}
                </View>
            </ScrollView>
            <TouchableOpacity onPress={()=>this.logout()} style={styles.logOut_style}>
                <Text style={{color:'white'}}>Log out</Text>
            </TouchableOpacity>
            </LinearGradient>
        )
    }
}


const styles = StyleSheet.create({
    modal_style:{
        // alignItems:'center',
        height:'80%',
        flex:1 ,
        flexDirection:'column',
    },
    close_button:{
        left:'40%',
        top:20,
    },
    user_info:{
        width:'90%',
        marginLeft:'auto',
        marginRight:'auto',
        // height:'40%',
        top:0,
        marginBottom:10,
        justifyContent: 'center',
        alignItems:'center',
    },
    user_name:{
        justifyContent: 'center',
        alignItems:'center',
        marginTop:10,
    },
    picture_style:{
        backgroundColor:'white',
        width:100,
        marginTop:10,
        height:100,
        borderRadius:55,
    },
    user_work:{
        marginTop:30,
        width:'90%',
        // height:'35%',
        marginLeft:'auto',
        marginRight:'auto',
    },
    permission_style:{
        backgroundColor:'#3d69e6',
        width:'100%',
        height:'30%',
        marginBottom:10,
        borderRadius:5,
        justifyContent:'center'
    },
    logOut_style:{
        width:'100%',
        height:'10%',
        backgroundColor:'#3d69e6',
        bottom:0,
        position:'absolute',
        justifyContent: 'center',
        alignItems:'center',
    }
});
                    {/* <View style={styles.permission_style}>
                        <Text style={{color:'white',paddingLeft:10,fontSize:18}}>Support</Text>
                    </View> */}