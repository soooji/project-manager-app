import React, { Component } from 'react';
import RF from 'react-native-responsive-fontsize'
import Spinner from 'react-native-loading-spinner-overlay';
import CommentModal from './CommentModal'
import Modal from "react-native-modal";
import IconF from 'react-native-vector-icons/FontAwesome5';
import { Platform,Dimensions,ImageBackground,TouchableOpacity,View,Text ,StyleSheet ,Animated,Easing,AsyncStorage,ScrollView,BackHandler} from 'react-native';
import LinearGradient from 'react-native-linear-gradient';
import fontelloConfig from './config.json';
import { createIconSetFromFontello } from 'react-native-vector-icons';
import { Right } from 'native-base';
import axios from 'axios'
const WIN = Dimensions.get('window');
const Icon = createIconSetFromFontello(fontelloConfig);
import {logRequests} from 'react-native-requests-logger';
export default class CommentsPage extends Component{
    static navigationOptions = {
        header: null
    }
    constructor(props){
    super(props);
    logRequests();
        this.state={
            isLoading:false,

            isVisible : false,
            isVisible_doneinfo: false,
            current_instruction_id: 0,
            product_instruction: {
                done_by: {
                    first_name: '',
                    last_name: ''
                },
                done_on: 0
            },
            plan: {
                id: 0,
                url: ''
            },
            isLoading:true,
            description: '',
            part_instructions:[],
            part_details: [],
            hw:new Animated.Value(120),
            detailsVisiblity: false,
            url: '',
            name: '',
            factory: {
                title: ''
            }
        }
    }
    componentDidMount(){
        this.getData();
        BackHandler.addEventListener('hardwareBackPress', this.handleBackPress);
    }
    componentWillUnmount() {
        BackHandler.removeEventListener('hardwareBackPress', this.handleBackPress);
    }
    handleBackPress = () => {
        this.props.navigation.navigate('page2');
        return true;
    }
    getData = async() => {
        this.setState({isLoading:true});
        const token = await AsyncStorage.getItem('token');
        axios.get(`http://dev.pm.coleo.me/api/products/${this.props.navigation.getParam('product_id')}/${token}/`)
        .then(r=>{
            AsyncStorage.setItem('token',r.data.token);
            this.setState({
                url: r.data.product.part.logo.url,
                factory: r.data.product.part.factory,
                part_instructions: r.data.instructions,
                name: r.data.product.part.name,
                description: r.data.product.part.description,
                part_details: r.data.product.part.part_details,
                installation_item: r.data.product.part.installation_item,
                installation_package: r.data.product.part.installation_package,
            })
            console.log(r.data);
        })
        .then(r=>this.setState({isLoading:false}))
        .catch(e=>{
            this.setState({isLoading:false});
            if(e.response) {
                if(e.response.status == 404) {
                    alert(`No product found with id :${this.props.navigation.getParam('product_id')}`)
                } else {
                    alert(e);
                }
            } else {
                alert("Something went wrong!");
            }
        });
      }
      doneTask = async(id) => {
          this.setState({isLoading:true});
        const token = await AsyncStorage.getItem('token');
        axios.get(`http://dev.pm.coleo.me/api/products/${this.props.navigation.getParam('product_id')}/instructions/${id}/done/${token}/`)
        .then(r=>{
            AsyncStorage.setItem('token',r.data.token);
        })
        .then(r=>this.setState({isLoading:false}))
        .then(r=>this.getData())
        .then(r=>{
            alert('task is done!')
        })
        .catch(e=>{
            this.setState({isLoading:false})
            alert(e)
        });
      }
    render(){
        return(
        <ImageBackground style={styles.main} source={require('./../images/white-towers.png')} resizeMode={'contain'}>
        <Spinner
          visible={this.state.isLoading}
          textContent={'Loading...'}
          textStyle={{color: '#FFF'}}
        />
        <Modal
            isVisible={this.state.isVisible_doneinfo}
            onBackdropPress={() => this.setState({ isVisible_doneinfo: false })}
            onSwipe={() => this.setState({ isVisible_doneinfo: false })}
            backdropOpacity={.4}
            style={{justifyContent: "flex-end",margin: 0}}
        >
            <View style={[styles.modalInnerBox,{backgroundColor:'white',}]}>
                <View style={{paddingVertical:17,paddingHorizontal:25,flexDirection:'row',width:'100%',justifyContent:'center',display:'flex',alignContent:'center'}}>
                    
                    <View style={{width:'100%',flexDirection:'row',justifyContent:'flex-start',display:'flex',alignContent:'flex-start'}}>
                        {/* <IconF name="sort" size={RF(2.7)}></IconF> */}
                        <Text style={{textAlign:'right',fontSize:RF(3.2),paddingRight:10,fontFamily:'K2D-Bold'}}>Task information</Text>
                    </View>
                    
                    <TouchableOpacity onPress={()=>this.setState({isVisible_doneinfo:false})} style={{}}><IconF name="times-circle" size={RF(3.3)} color="rgba(0,0,0,.3)"/></TouchableOpacity>
                </View>
                {
                    this.state.product_instruction != {} ? 
                    
                    <View style={{paddingHorizontal:10}}>
                    <View style={{backgroundColor:'rgba(0,0,0,0.1)',marginBottom:3,padding:10,borderRadius:5}}>
                        <Text style={{fontSize:RF(2.5)}}>
                            Done by : <Text style={{fontFamily:'K2D-Bold'}}>{this.state.product_instruction.done_by.first_name + " " + this.state.product_instruction.done_by.last_name}</Text>
                        </Text>
                    </View>
                    <View style={{backgroundColor:'rgba(0,0,0,0.1)',marginBottom:3,padding:10,borderRadius:5}}>
                        <Text style={{fontSize:RF(2.5)}}>
                            On : <Text style={{fontFamily:'K2D-Bold'}}>{this.state.product_instruction.done_one}</Text>
                        </Text>
                    </View>
                </View> : <View style={{paddingHorizontal:10}}></View>
                }
                
            </View>
        </Modal>
        <Modal
            isVisible={this.state.isVisible}
            onBackdropPress={() => this.setState({ isVisible: false })}
            onSwipe={() => this.setState({ isVisible: false })}
            backdropOpacity={.4}
            style={{justifyContent: "flex-end",margin: 0}}
        >
            <View style={[styles.modalInnerBox,{backgroundColor:'white',}]}>
                <View style={{
                    paddingVertical:17,
                    paddingHorizontal:25,flexDirection:'row',width:'100%',justifyContent:'center',display:'flex',alignContent:'center'}}>
                    
                    <View style={{width:'100%',flexDirection:'row',justifyContent:'flex-start',display:'flex',alignContent:'flex-start'}}>
                        {/* <IconF name="sort" size={RF(2.7)}></IconF> */}
                        <Text style={{textAlign:'right',fontSize:RF(3.2),paddingRight:10,fontFamily:'K2D-Bold'}}>Comments</Text>
                    </View>
                    
                    <TouchableOpacity onPress={()=>this.setState({isVisible:false})} style={{}}><IconF name="times-circle" size={RF(3.3)} color="rgba(0,0,0,.3)"/></TouchableOpacity>
                </View>
                <View>
                    <CommentModal instructionid={this.state.current_instruction_id} productid={this.props.navigation.getParam('product_id')}/>
                </View>
            </View>
        </Modal>
            <ScrollView
                showsVerticalScrollIndicator={false}
                horizontal={false}
                bounces={false}
                // onScroll={e=>{}}
                // onMomentumScrollBegin={()=>this.changeAnime(40)}
                // onResponderStart={()=>this.changeAnime(40)}
                // onResponderEnd={()=>this.changeAnime(120)}
                // onMomentumScrollEnd={()=>this.changeAnime(120)}
                stickyHeaderIndices={[0]}
            >
                {/* <LinearGradient colors={['#33B66A','#38A866']} style={styles.topPage}> */}
                <View style={{backgroundColor:'#38A866'}}>
                    <View style={styles.icons}>
                        <TouchableOpacity transparent onPress={() =>this.props.navigation.navigate('page2')}>
                            <View style = {{flexDirection:'row',alignItems:'center'}}>
                                <Icon style ={{color:'white'}} size={15} name='left-arrow'/>
                                <Text style={{color:'#f5f5ef',fontSize:17,paddingLeft:7}}>Return</Text>
                            </View>
                        </TouchableOpacity>
                        <View style={{flexDirection:'row-reverse',alignContent:'center',alignItems:'center'}}>
                            <TouchableOpacity style={{paddingTop:6,left:10}} onPress={() =>this.props.navigation.navigate('page5',{
                                product_id: this.props.navigation.getParam('product_id')
                            })}>
                                <IconF style={{color:'white'}} size={24} name='comment' />
                            </TouchableOpacity>
                        </View>
                    </View>
                    <View style={styles.product_info}>
                        <View style={[styles.product_Image,{width:'auto',height:'auto'}]}>
                            <Animated.Image style={{width:this.state.hw,height:this.state.hw}} source={{uri:`http://dev.pm.coleo.me${this.state.url}`}} /> 
                        </View>
                        <View style={styles.product_name}>
                            <Text style={{color:'white',fontSize:RF(3.2),fontFamily:'K2D-Bold'}}>{this.state.name}</Text>
                            <Text style={{color:'rgba(255,255,255,.6)',fontSize:RF(2),fontFamily:'K2D-Regular',paddingBottom:20}}>Factory: {this.state.factory.title}</Text>
                            <Text style={{color:'rgba(255,255,255,.9)',fontSize:RF(2.8),fontFamily:'K2D-Regular'}}>Product id: {this.props.navigation.getParam('product_id')}</Text>
                            <TouchableOpacity onPress={()=>this.setState({detailsVisiblity:!this.state.detailsVisiblity})} style={{paddingHorizontal:10,paddingVertical:6,backgroundColor:'#00CE7D',borderRadius:30,marginRight:'auto',marginBottom:10,marginTop:10}}>
                                <Text style={{color:'white',fontSize:RF(1.9)}}>{this.state.detailsVisiblity ? 'Tasks' : 'Details'}</Text>
                            </TouchableOpacity>
                        </View>
                    </View>
                    <View style={{width:'100%',paddingLeft:10,paddingRight:10,flexDirection:'row',justifyContent:'space-between',alignItems:'center',alignContent:'center',paddingBottom:15}}>
                        <TouchableOpacity style={{borderRadius:25,paddingVertical:15,width:'48%',backgroundColor:'rgba(255,255,255,.3)',alignItems:'center',justifyContent:'center'}} onPress={() =>this.props.navigation.navigate('page9',{
                            product_id: this.props.navigation.getParam('product_id'),
                            plan: `http://dev.pm.coleo.me${this.state.installation_package.url}`
                        })}>
                            <Text style={{color:'white',textAlign:'center',fontSize:14}}>Installation Package</Text>
                        </TouchableOpacity>
                        <TouchableOpacity style={{borderRadius:25,paddingVertical:15,width:'48%',backgroundColor:'rgba(255,255,255,.3)',alignItems:'center',justifyContent:'center'}} onPress={() =>this.props.navigation.navigate('page9',{
                            product_id: this.props.navigation.getParam('product_id'),
                            plan: `http://dev.pm.coleo.me${this.state.installation_item.url}`
                        })}>
                            <Text style={{color:'white',textAlign:'center',fontSize:14}}>Installation Item</Text>
                        </TouchableOpacity>
                    </View>
                {/* </LinearGradient> */}
                </View>
                <View style={[styles.product_step,{backgroundColor:'rgba(255,255,255,.2)'}]}>
                {this.state.detailsVisiblity ?

                    <View style={{
                            // height:'auto',
                            backgroundColor:'white',
                            borderTopLeftRadius:10,
                            borderTopRightRadius:10,
                            // marginLeft:20,
                            // marginRight:20,
                            // marginTop:15,
                            padding:15,
                            shadowOffset:{width: 0,height: -6,},
                            shadowColor: 'rgba(0,0,0,1)',
                            shadowOpacity: .08,
                            // elevation:1
                        }}>
                    <Text style={{fontSize:RF(2.7),fontFamily:'K2D-Bold'}}>
                        Properties:
                    </Text>
                    <View style={{paddingTop:10,paddingBottom:15}}>
                    {
                        this.state.part_details.map(
                            (v,k) =>
                                <View style={{backgroundColor:'rgba(0,0,0,0.1)',marginBottom:3,padding:10,borderRadius:5}} key={k}>
                                    <Text style={{fontSize:RF(2.5),fontFamily:'K2D-Regular'}}>
                                        {`${v.part_property.name} : ${v.value}`}
                                    </Text>
                                </View>
                            
                        )
                    }
                    </View>
                    <Text style={{fontSize:RF(2.7),paddingBottom:10,fontFamily:'K2D-Bold'}}>
                        Description:
                    </Text>
                    <Text style={{fontSize:RF(2.5),fontFamily:'K2D-Regular'}}>
                        {this.state.description}
                    </Text>
                    </View>
                : 
                <View>
                    {this.state.part_instructions.map(
                        (v,k)=>
                        <View key={k} style={styles.box}>
                            <View>
                                <Text style={styles.nameBox}>{v.name}</Text>
                                <Text style={styles.detail}>{v.description}</Text>
                            </View>
                            <View style={{display:'flex',flexDirection:'row'}}>
                            
                                <TouchableOpacity onPress={()=>this.setState({current_instruction_id: v.id,isVisible_doneinfo: true,product_instruction: v.product_instruction ? v.product_instruction : {}})} style={{display: v.product_instruction ? 'flex' : 'none',marginRight:4}}>
                                    <LinearGradient colors={['#33B66A','#38A866']} style={{paddingHorizontal:10,paddingVertical:8,borderRadius:40}}>
                                        <IconF name="check" size={RF(2.1)} color="white"/>
                                    </LinearGradient>
                                </TouchableOpacity>

                                <TouchableOpacity onPress={()=>this.doneTask(v.id)} style={{display: v.is_mitoone && !v.product_instruction ? 'flex' : 'none',marginRight:4}}>
                                    <LinearGradient colors={['#33B66A','#38A866']} style={{paddingHorizontal:10,paddingVertical:8,borderRadius:40}}>
                                        <Text style={{color:'white',fontSize:RF(1.9)}}>Done</Text>
                                    </LinearGradient>
                                </TouchableOpacity>

                                <TouchableOpacity style={{marginRight:0}} onPress={()=>this.setState({current_instruction_id: v.id,isVisible: true})}>
                                    <LinearGradient colors={['#698FFA','#446EF6']} style={{paddingHorizontal:10,paddingVertical:8,borderRadius:40}}>
                                        {/* <Text style={{color:'white',fontSize:RF(1.9)}}>Comment</Text> */}
                                        <IconF name="comment" size={RF(2.1)} color="white"/>
                                    </LinearGradient>
                                </TouchableOpacity>
                               
                            </View>
                        </View>
                    )}
                </View>
                }
                </View>
            </ScrollView>
        </ImageBackground>
         )

    }
}
const styles = StyleSheet.create({
    modalInnerBox: {
        marginTop:'auto',
        width:'100%',
        height:'auto',
        // paddingBottom:20,
        borderTopLeftRadius:25,
        borderTopRightRadius:25
    },
    main: {
        width: '100%',
        height: '100%',
        display: 'flex',
        flexDirection: 'column',
        flexWrap: 'nowrap',
        justifyContent: 'space-between',
        alignItems: 'stretch',
        alignContent: 'stretch'
    },
    container:{
        backgroundColor:'#e6e6ff',
        flex:1,
    },
    topPage:{
        height:'auto',
        width:'100%',
        // position:'absolute',
        // zIndex:111111111111111
    },
    icons:{
        top:Platform.OS=="ios"? 27 : 20, 
        paddingHorizontal:20,
        paddingVertical:10,
        // width:'86%',
        // left:'7%',
        justifyContent:'space-between',
        flexDirection:'row',
        alignItems:'center'
    },
    center:{
        width:WIN.width,
        height:WIN.height,
        position:'absolute',
        zIndex:11,
        bottom:0,
    },
    product_info:{
        padding:20,
        marginTop:15,
        width:'100%',
        flexDirection:'row',
    },
    product_Image:{
        backgroundColor:'white',
        justifyContent:'center',
        padding:10,
        borderRadius:5,
        marginRight:20,
    },
    product_name:{
        // width:'55%',
        flexDirection:'column',
        justifyContent:'flex-end',
    },
    product_step:{
        // marginTop:'10%',
        // position:'absolute',
        // marginTop:10,
        height:'auto',
        width:'100%',
        // backgroundColor:'red',
        paddingBottom:25,
        paddingHorizontal:14
    },
    box:{
        display: 'flex',
        flexDirection: 'row',
        flexWrap: 'nowrap',
        justifyContent: 'space-between',
        alignItems: 'center',
        alignContent: 'center',
        margin :7,
        backgroundColor : 'white',
        padding:10,
        borderRadius: 4,
        borderLeftWidth:3,
        borderLeftColor:'#D2D2D2',
        shadowOffset:{  width: 0,  height: 6,  },
        shadowColor: 'rgba(0,0,0,.06)',
        shadowOpacity: 3.0,  
        elevation:1 ,

    },
    nameBox:{ 
        color:'black',
        fontSize:18,   
        fontFamily:'K2D-Bold'
    },
    detail : {
        fontSize : 13,
        color: 'rgba(0,0,0,.3)',
        fontFamily:'K2D-Regular'
    },
})
