import React, { Component } from 'react';
import {
  ActivityIndicator,
  ImageBackground,
  Text
} from 'react-native';
import { createIconSetFromFontello } from 'react-native-vector-icons';
import fontelloConfig from './config.json';
const Icon = createIconSetFromFontello(fontelloConfig);
import RF from 'react-native-responsive-fontsize'
export default class SplashScreen extends Component{

    render() {
    return (
        <ImageBackground source={require('./../images/photo_2018-11-20_19-44-08.jpg')} style={{width: '100%', height: '100%'}}>
            <Icon color="white" style={{marginLeft:'auto',marginRight:'auto',marginTop:130}} size={RF(13)} name='p'/>
            <ActivityIndicator style={{marginTop:40}} color="white"/>
        </ImageBackground>
    );
  }
}