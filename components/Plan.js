import React, { Component } from 'react'
import {View, TouchableOpacity } from 'react-native'
import PhotoView from 'react-native-photo-view';

import Icon from 'react-native-vector-icons/FontAwesome5';
import RF from 'react-native-responsive-fontsize'
import {Dimensions,StyleSheet} from 'react-native'
const WIN = Dimensions.get('window');
// constants end
export default class Market extends Component {
    constructor(props) {
        super(props);
        this.state = {
            images: [],
            product_id: 0,
            plan: ''
        }
    }
    componentDidMount() {
        this.setState({
            product_id: this.props.navigation.getParam('product_id'),
            plan: this.props.navigation.getParam('plan'),
        })
    }
    static navigationOptions = {
        header: null,
    };
  render() {
    return (
      <View>
          {/* 'https://via.placeholder.com/500x500.png?text=No+image' */}
            <PhotoView
                source={{uri: this.state.plan}}
                minimumZoomScale={1}
                maximumZoomScale={3}
                androidScaleType="center"
                // onLoad={() => console.log("Image loaded!")}
                style={{width: WIN.width, height: WIN.height,backgroundColor:'black'}}>
                </PhotoView>
                <View style={{position:'absolute',top:0,width:WIN.width,padding:0,backgroundColor:'rgba(255,255,255,.2)',display:'flex',flexDirection:'row-reverse',justifyContent:'space-between',alignContent:'center'}}>
                    <TouchableOpacity style={{padding:10}}>
                        {/* <Icon name="heart" size={RF(4)} color="white"/> */}
                    </TouchableOpacity>
                    <TouchableOpacity style={{paddingHorizontal:15,paddingTop:25,paddingBottom:10}} onPress={() =>this.props.navigation.navigate('page3',{product_id: this.state.product_id})}>
                        <Icon name="chevron-left" size={RF(3.6)} color="white"/>
                    </TouchableOpacity>
                </View>
                {/* <View style={{position:'absolute',bottom:0,width:WIN.width,padding:0,backgroundColor:'rgba(255,255,255,.2)',display:'flex',flexDirection:'row-reverse',justifyContent:'space-between',alignContent:'center'}}>
                    <TouchableOpacity style={{paddingHorizontal:15,paddingVertical:10}}>
                        <Icon name="heart" size={RF(4)} color="white"/>
                    </TouchableOpacity>
                    <TouchableOpacity style={{padding:10}}>
                        <Icon name="share-alt" size={RF(4)} color="white"/>
                    </TouchableOpacity>
                </View> */}
      </View>
    )
  }
}
const styles = StyleSheet.create({

})