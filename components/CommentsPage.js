import React, { Component } from 'react';
import axios from 'axios'
import RF from 'react-native-responsive-fontsize'
import {
  StyleSheet,
  Text,
  Platform,
  View,
  TouchableOpacity,
  AsyncStorage,
  Image,
  ScrollView,
  Button,
  FlatList,
  TextInput,
  ImageBackground,
  Dimensions,
  Keyboard,
  TouchableHighlight,
  ActivityIndicator,
  InputAccessoryView,
  KeyboardAvoidingView
} from 'react-native';
import LinearGradient from 'react-native-linear-gradient';
import fontelloConfig from './config.json';
import { createIconSetFromFontello } from 'react-native-vector-icons';

const Icon = createIconSetFromFontello(fontelloConfig);

export default class CommentsPage extends Component {
    static navigationOptions = {
        header: null
    }

  constructor(props) {
    super(props);
    this.state={
      text:'',
        isLoading:false,
        product_id: '',
        comments: [],
    }
  }

  componentDidMount(){
    this.setState({
      product_id: this.props.navigation.getParam('product_id')
    })
    this.getMessages();
  }
  getMessages = async()=> {
    this.setState({isLoading:true})
    const token = await AsyncStorage.getItem('token');
    axios.get(`http://dev.pm.coleo.me/api/products/${this.props.navigation.getParam('product_id')}/comments/${token}/`)
    .then(r=>{
      this.setState({
        comments: r.data.comments,
        isLoading:false
      })
      console.log(r.data.comments)
    })
    .catch(e=>{
      this.setState({isLoading:false})
      console.log(e);
    })
  }
  sendMessage = async()=> {
    const token = await AsyncStorage.getItem('token');
    axios.post(`http://dev.pm.coleo.me/api/products/${this.props.navigation.getParam('product_id')}/comments/${token}/`,{
      comment: {
        text: this.state.text
      }
    })
    .then(r=>{
      this.setState({
        text: ''
      })
    })
    .then(r=>{
      this.refs.flatlist.scrollToEnd();
      Keyboard.dismiss();
      this.getMessages();
    })
    .catch(e=>{
      alert(e);
      console.log(e);
    })
  } 
getRandomColor() {
  var letters = '0123456789ABCDEF';
  var color = '#';
  for (var i = 0; i < 6; i++) {
    color += letters[Math.floor(Math.random() * 16)];
  }
  return color;
}
stringToHslColor(str, s, l) {
  var hash = 0;
  for (var i = 0; i < str.length; i++) {
    hash = str.charCodeAt(i) + ((hash << 5) - hash);
  }
  var h = hash % 360;
  return 'hsl('+h+', '+s+'%, '+l+'%)';
}
  render() {
    const {width} = Dimensions.get('window');
    return (
      <ImageBackground source={require('./../images/photo_2018-11-26_13-52-08.jpg')} style={{width: '100%', height: '100%'}}>
          <LinearGradient colors={['#33B66A','#38A866']} style={styles.topPage} >
            {/* <View style={styles.icons}>
              <TouchableOpacity transparent onPress={() =>this.props.navigation.navigate('page3')}>
              <View style = {{flexDirection:'row',alignItems:'center'}}>
                  <Icon style ={{color:'white'}} name='left-arrow'/>
                  <Text style={{width:200 ,color:'#f5f5ef',fontSize:17}}>Product Page </Text>
              </View>
              </TouchableOpacity>
            </View> */}
            <View style={styles.icons2}>
                <TouchableOpacity style={{paddingTop:10}} transparent onPress={() =>this.props.navigation.navigate('page3',{product_id: this.state.product_id})}>
                    <View style = {{flexDirection:'row',alignItems:'center'}}>
                        <Icon style ={{color:'white'}} size={15} name='left-arrow'/>
                        <Text style={{color:'#f5f5ef',fontSize:17,paddingLeft:7}}>Product Page</Text>
                    </View>
                </TouchableOpacity>
                <TouchableOpacity style={{paddingTop:6}} onPress={() =>this.props.navigation.navigate('page5')}>
                    {/* <Icon style={{color:'white'}} size={30} name='chat' /> */}
                </TouchableOpacity>
            </View>
          </LinearGradient>
          <View style={styles.center}>
            <View style={styles.titleBox}>
              <Text style={styles.title}>Comments</Text>
            </View>
            {/* {!this.state.isLoading ? */}
            {
              this.state.comments.length == 0 ?
              <View style={{top:100,zIndex:1111}}>
                <Text style={{width:'100%',textAlign:'center',bottom:40,fontFamily:'K2D-Regular',fontSize:RF(2.5)}}>
                    No comment!
                </Text>
              </View> : null
            }
              <FlatList
              ref="flatlist"
              keyboardDismissMode="interactive"
              style={styles.root}
              data={this.state.comments}
              extraData={this.state}
              renderItem={(item) => {
                return(
                  <View style={styles.container}>
                  {console.log(item)}
                    <View style={styles.content}>
                      <LinearGradient style={[styles.image,{justifyContent:'center',alignItems:'center'}]} colors={[this.stringToHslColor(item.item.user.first_name+item.item.user.last_name,30,80),this.stringToHslColor(item.item.user.first_name+item.item.user.last_name,80,40)]} >
                      <Text style={{color:'white',fontFamily:'K2D-Regular',fontSize:24,textAlign:'center'}}>{item.item.user.first_name.slice(0,1)}</Text>
                      </LinearGradient>
                      <View style={styles.contentHeader}>
                        <Text style={styles.name}>{item.item.user.first_name+' '+item.item.user.last_name}</Text>
                      </View>
                      <Text style={{fontSize:13,color:'white',paddingLeft:5,fontFamily:'K2D-Regular',left:60,position:'absolute',top:5}}>{item.item.user.username}</Text>
                      <View style={styles.commentBox}> 
                        <Text rkType='primary3 mediumLine' style={{color:'white',fontFamily:'K2D-Regular',fontSize:16}}>{item.item.text}</Text>
                      </View>
                    </View>
                  </View>
                );
              }}/>
               {/* : <View style={[styles.root,styles.container]}><ActivityIndicator style={{marginTop:40}} color="black"/></View>} */}
          

        {/* <InputAccessoryView>
            <View style={[styles.textInputContainer,styles.send_message, {width,padding:10}]}>
            <TextInput
                placeholder="Type here your comment..."
                onChangeText={(text) => this.setState({text})}
                enablesReturnKeyAutomatically
                pointerEvents="auto"
                multiline={true}
                style={{paddingHorizontal:15,paddingBottom:14,paddingTop:14,width:width-70,fontSize:16,backgroundColor:'#F5F7FA',borderRadius:25}}
                value={this.state.text}
              />
            <TouchableOpacity onPress={()=>this.sendMessage()} style={{backgroundColor:'#3AD330',borderRadius:40,padding:10,bottom:3}}>
              <Icon style={{textAlign:'center',color:'white',top:1,right:1}} size={20} name="send"></Icon>
            </TouchableOpacity>
          </View>
        </InputAccessoryView>  */}

        </View>
        </ImageBackground>
    );
  }
}
class TextInputBar extends Component {
  state = {text: ''};

  render() {
    const {width} = Dimensions.get('window');
    return (
      <View style={[styles.textInputContainer,styles.send_message, {width,padding:10}]}>
        <TextInput
            placeholder="Type here your comment..."
            onChangeText={(text) => this.setState({text})}
            enablesReturnKeyAutomatically
            pointerEvents="auto"
            multiline={true}
            style={{paddingHorizontal:15,paddingBottom:14,paddingTop:14,width:width-70,fontSize:16,backgroundColor:'#F5F7FA',borderRadius:25}}
            value={this.state.text}
          />
        <TouchableOpacity style={{backgroundColor:'#3AD330',borderRadius:40,padding:10,bottom:3}}>
          <Icon style={{textAlign:'center',color:'white',top:1,right:1}} size={20} name="send" onPress={()=>this.props.navigation.navigate('page3')}></Icon>
        </TouchableOpacity>
      </View>
    );
  }
}
const styles = StyleSheet.create({
  textInputContainer: {
    flexDirection: 'row',
    justifyContent:'space-between',
    alignItems:'flex-end',
    borderTopLeftRadius:20,
    borderTopRightRadius:20
  },
  textInput: {
    flex: 1,
    paddingLeft: 10,
  },
  text: {
    padding: 10,
    color: 'white',
  },
  textBubbleBackground: {
    backgroundColor: '#2f7bf6',
    borderRadius: 20,
    width: 110,
    margin: 20,
  },
  root: {
    borderTopLeftRadius:10,
    borderTopRightRadius:10,
    backgroundColor: "#ffffff",
    marginBottom:0,
  },

  topPage:{
    height:'30%',
    backgroundColor:'#34B269',
    alignItems:'center',
},
  icons2:{
    top:Platform.OS=="ios"? 27 : 20, 
    paddingHorizontal:20,
    paddingVertical:10,
    width:'100%',
    // left:'7%',
    justifyContent:'space-between',
    flexDirection:'row',
    alignItems:'center'
    // top:20,
    // width:'86%',
    // justifyContent:'space-between',
    // flexDirection:'row',
  },
  center:{
    width:'100%',
    height:'90%',
    position:'absolute',
    marginTop:10,
    bottom:0,
    paddingLeft:15,
    paddingRight:15,
    flexDirection:'column',
},
container: {
  marginTop:30,
  marginBottom:10,
  marginHorizontal:2,
  paddingLeft:10,
  paddingRight:10,
  flexDirection: 'column',
},
  content: {
    backgroundColor:'#517AF8',
    borderRadius:10,
  },
  image:{
    width:50,
    height:50,
    borderRadius:25,
    position:'absolute',
    top:-22.5,
    left:10
  },
  send_message:{
    width:'100%',
    // height: 50,
    elevation:5,
    // position:'absolute',
    // bottom:0,
    backgroundColor:'white',
    shadowOffset:{width: 0,height: -3},
    shadowColor: 'rgba(0,0,0,.05)',
    shadowOpacity: 3.0,
},
  contentHeader: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    marginBottom: 6,
    left:60,
    top:-19,
  },
  separator: {
    height: 1,
    backgroundColor: "gray"
  },

  time:{
    fontSize:11,
    color:"#808080",
  },
  commentBox:{
    padding:15
  },
  name:{
    fontSize:16,
    color:'#5B6378',
    paddingLeft:5,
    fontFamily:'K2D-Bold'
  },
  titleBox:{
    justifyContent:'center',
    alignItems:'center',
    paddingTop:10
  },
  title:{
    fontSize:23,
    fontWeight:"bold",
    color:'white',
    marginBottom:25,
    fontFamily:'K2D-Bold'
  }
});
        {/* <InputAccessoryView style={styles.send_message} nativeID={inputAccessoryViewID}> */}
        {/* <InputAccessoryView nativeID={inputAccessoryViewID}> */}
          {/* <TextInput
            inputAccessoryViewID={inputAccessoryViewID}
            placeholder="Type here your comment..."
            onChangeText={(text) => this.setState({text})}
            enablesReturnKeyAutomatically
            pointerEvents="auto"
            style={{position:'absolute',padding:15,width:'100%'}}
          /> */}
          {/* <View style={{backgroundColor: 'white'}}> */}
            {/* <Button
              onPress={() => this.setState({text: 'Placeholder Text'})}
              title="Reset Text"
            /> */}
          {/* </View> */}
        {/* </InputAccessoryView> */}
          {/* <Icon style={{right:15,top:15,position:'absolute',color:'gray'}} size={20} name="send" onPress={()=>this.props.navigation.navigate('page3')}></Icon> */}
          {/* </View> */}
        {/* </InputAccessoryView> */}