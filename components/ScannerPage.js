import React, { Component } from 'react';

import {Dimensions, View,Text ,StyleSheet,ImageBackground,Image ,TouchableHighlight,TouchableOpacity,Modal,Platform,BackHandler} from 'react-native';
import Profile from './Profile'
import LinearGradient from 'react-native-linear-gradient';
import { createIconSetFromFontello } from 'react-native-vector-icons';
import fontelloConfig from './config.json';
import RF from 'react-native-responsive-fontsize'
const WIN = Dimensions.get('window');
const Icon = createIconSetFromFontello(fontelloConfig);
export default class ScannerPage extends Component{
    constructor(props) {
        super(props);
        this.state = {
            modalVisible: false,
            isVisible : true,
        }
    }
    static navigationOptions = {
        header: null
    }
    setModalVisible(visible) {
        this.setState({modalVisible: visible});
    }
    componentDidMount(){
        BackHandler.addEventListener('hardwareBackPress', this.handleBackPress);
    }
    componentWillUnmount() {
        BackHandler.removeEventListener('hardwareBackPress', this.handleBackPress);
    }
    handleBackPress = () => {
        this.setModalVisible(false);
        return true;
    }
    render(){
        // const { navigate } =this.props.navigation;
        return(
            <View style={styles.container}>
             <ImageBackground source={require('./../images/white-towers.png')} style={{width: '100%', height: '100%'}} resizeMode={'contain'} resizeMethod={'auto'}>
                <Modal
                animationType="slide"
                transparent={false}
                visible={this.state.modalVisible}>
                    <TouchableOpacity activeOpacity={1} style={[styles.close_button,{paddingTop:Platform.OS == "android"? 15: 35,padding:23}]} onPress={() => this.setModalVisible(false)}>
                        <Icon color="white" style={{marginLeft:'auto'}} size={18} name='close'/>
                    </TouchableOpacity>
                    <Profile navigation={this.props.navigation}/>
                </Modal>
                <LinearGradient colors={['#698FFA','#446EF6']} style={styles.topPage} >
                    <TouchableOpacity style={{top:Platform.OS == "android"? 0: 20,padding:23}}>
                        <Icon color="white" style={{marginLeft:'auto',marginTop:-10}} size={30} name='p'/>
                    </TouchableOpacity>
                    <TouchableOpacity style={{top:Platform.OS == "android"? 0: 20,padding:23}} onPress={() => this.setModalVisible(true)}>
                        <Icon color="white" style={{marginLeft:'auto'}} size={18} name='menu'/>
                    </TouchableOpacity>                    
                </LinearGradient>
                <View style={styles.center}>
                    <Text style={{backgroundColor:'#3E6CF5',fontSize:20 ,textAlign:'center',color:'white' ,padding:5}}>Please scan product QRcode</Text>
                    <View style={styles.QRcode}>
                        <View style={styles.QRcode_Image}>
                            <Image source={require('./../images/qr.png')} resizeMode={'contain'} style={{width: '100%', height: '100%'}} />
                        </View>
                    </View>
                    <TouchableOpacity activeOpacity={.9} style={styles.scanButton} onPress={() =>this.props.navigation.navigate('page8')}>
                        <Text style={{justifyContent:'center' ,alignItems:'center',color:'white',fontSize:23,fontFamily:'K2D-Regular'}}>Start scanning</Text>
                    </TouchableOpacity>
                    <View style={styles.description}>
                        <Text style={styles.textDescription}>
                        

                        </Text>
                    </View>
                </View>
            </ImageBackground>
            </View>
        )
    }
    
}
const styles = StyleSheet.create({
    container:{
        backgroundColor:'#f4f8fb',
        flex:1,
    },
    icon:{
        color:'#f5f5ef',
        left:'90%',
        top:'10%'
    },
    close_button:{
        backgroundColor:'#698ffa',
        // height:'7%'
    },
    topPage:{
        height:'40%',
        width:'100%',
        display:'flex',
        justifyContent:'space-between',
        flexDirection:'row'
    },
    center:{
        width:'80%',
        height:'87%',
        position:'absolute',
        top:110,
        
        bottom:0,
        left:'10%',
        flexDirection:'column',
        alignItems:'center'
    },
    QRcode:{
        marginTop:10,
        width:WIN.width - 180,
        height:WIN.width - 180,
        backgroundColor:'white',
        borderRadius:5,
        marginTop:20,
        alignItems:'center',
        justifyContent: 'space-between',
    },
    QRcode_Image:{
        width:'80%',
        height:'80%',
        marginTop:20,
        alignItems:'center',
        justifyContent: 'space-between',
    },
    scanButton:{
        width:'65%',
        height:'10%',
        marginTop:20,
        backgroundColor:'#2DC76D',
        justifyContent:'center' ,
        alignItems:'center',
        borderRadius:5,
        shadowOffset:{width: 0,height: 6},
        shadowColor: 'rgba(0,0,0,.1)',
        shadowOpacity: .6,  
        elevation:1 ,
    },
    description:{
        width:'90%',
        height:'35%',
        marginTop:20,
        margin:10
    },
    textDescription:{
        color:'#beb9b9',
        fontSize:18,
        textAlign: 'justify'
    }
})