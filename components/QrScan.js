import React, {Component} from 'react';
import { StyleSheet, View, Text, TouchableOpacity } from 'react-native';
import axios from 'axios'
import {QRscanner} from 'react-native-qr-scanner';
import RF from 'react-native-responsive-fontsize'
import fontelloConfig from './config.json';
import { createIconSetFromFontello } from 'react-native-vector-icons';
const Icon = createIconSetFromFontello(fontelloConfig);
export default class QrScan extends Component {
  constructor(props) {
    super(props);
    this.state = {
      flashMode: false,
      zoom: 0,
      qr: true,
      srt: '',
      productId: ''
    }
  }
  getData = async(e) => {
    // const token = await AsyncStorage.getItem('token');
    this.props.navigation.navigate('page3', {
      product_id: e.data,
    })
    // axios.get(`http://pm.coleo.me/api/products/${e.data}/${token}/`)
    // .then(r=>{
    // })
    // .then()
  }
  render() {
    return (
      <View style={{width:'100%',height:'100%'}}>
        <QRscanner hintText={'Scan the QR code'} onRead={(e)=>{this.getData(e)}} flashMode={this.state.flashMode} zoom={this.state.zoom} finderY={50}/>
        <TouchableOpacity onPress={()=>this.props.navigation.navigate('page2')} style={{position:'absolute',bottom:0,backgroundColor:'rgba(255,255,255,.3)',borderRadius:40,paddingLeft:10,paddingRight:13,margin:10}}>
          <Text style={{textAlign:'center',color:'white',paddingBottom:10,paddingTop:10}}>
            <Icon color="white" size={RF(2.4)} name='left-arrow'/>
            <Text style={{fontSize:RF(2.4)}}>Return</Text>
          </Text>
        </TouchableOpacity>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  welcome: {
    fontSize: 20,
    textAlign: 'center',
    margin: 10,
  },
  instructions: {
    textAlign: 'center',
    color: '#333333',
    marginBottom: 5,
  },
});