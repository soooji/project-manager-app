import React, { Component } from 'react';
import {
  AsyncStorage
} from 'react-native';
import axios from 'axios'
import SplashScreen from './SplashScreen'
import Home from './Home'
import ScannerPage from './ScannerPage'
import Login from './Login'

export default class Application extends Component{
  constructor(props) {
    super(props);
  }
  static navigationOptions = {
    header: null
  }
  componentDidMount() {
    this.destination();
  }
  login(username,password){
    axios.post(`http://dev.pm.coleo.me/api/auth/sign_in/`,{
        user:{
          username:username,
          password:password,
        }
    })
    .then(res=>{
      AsyncStorage.multiSet([['username', username], ['password', password],['first_name',res.data.user.first_name],['last_name',res.data.user.last_name],['is_admin',res.data.user.is_admin.toString()],['token',res.data.token]])
    })
    .then(()=>this.props.navigation.navigate('page2'))
    .catch(e =>{
      this.props.navigation.navigate('page7');
    })
}
  destination = async()=> {
    try {
      const username = await AsyncStorage.getItem('username');
      const password = await AsyncStorage.getItem('password');
      if (username != null && username != "" && password != null && password != "") {
            this.login(username,password);
      } else {
        this.props.navigation.navigate('page7')
      }
     } catch (error) {
        alert(error);
        this.props.navigation.navigate('page7')
     }
  }
render() {
  return(
    <SplashScreen/>
  );
}
}