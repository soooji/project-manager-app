import React, { Component } from 'react';
import RF from 'react-native-responsive-fontsize'
import {
  Platform,
  StyleSheet,
  Keyboard,
  Text,
  View,
  Alert,
  TextInput,
  ImageBackground,
  TouchableOpacity,
  AsyncStorage,
  AppRegistry,
  TouchableWithoutFeedback
} from 'react-native';
// import ScannerPage from './ScannerPage';
import axios from 'axios';
import {logRequests} from 'react-native-requests-logger';
import { createIconSetFromFontello } from 'react-native-vector-icons';
import fontelloConfig from './config.json';
const Icon = createIconSetFromFontello(fontelloConfig);
export default class Login extends Component{

    constructor(){
        super();
        logRequests(); //log the requests
        this.state = {
            username: '',
            password:'',
            first_name: '',
            last_name: '',
            password_new: '',
            error: '',
            state: 'login' // login or signup
        }
    }
    requestSignUpFromApi(){
        if(this.state.username.length > 0) {
            axios.post(`http://dev.pm.coleo.me/api/pending_email/`,{
                email:this.state.username,
                first_name: this.state.first_name,
                last_name: this.state.last_name,
                password: this.state.password_new
            })
        .then(
            ()=>this.setState({error:'Thanks!',state: 'login'})
        )
        .catch(e => {
            this.setState({error:'failed to login!'})
            }
        )
        } else {
            this.setState({
                error:'Please fill all register!'
            })
        }
    }
    requestLoginFromApi(){
        if(this.state.username.length > 0 && this.state.password.length > 0) {
            axios.post(`http://dev.pm.coleo.me/api/auth/sign_in/`,{
                user:{
                username:this.state.username,
                password:this.state.password,
            }
        })
        .then(res=>{
            AsyncStorage.multiSet([['username', this.state.username],['factory', res.data.user.factory.title],['password', this.state.password],['first_name',res.data.user.first_name],['last_name',res.data.user.last_name],['is_admin',res.data.user.is_admin.toString()],['token',res.data.token]])
        })
        .then(
            ()=>this.props.navigation.navigate('page2')
        )
        .catch(e => {
            this.setState({error:'failed to login!'})
            }
        )
        } else {
            this.setState({
                error:'Please fill all fields!'
            })
        }
    }
    render() {
    return (
        <TouchableWithoutFeedback
        onPress={()=>Keyboard.dismiss()}
        style={{zIndex:1111111}}
        accessible={false}
        >
        <ImageBackground
        // keyboardShouldPersistTaps='handled'
        source={require('./../images/photo_2018-11-20_19-44-08.jpg')} style={{width: '100%', height: '100%' ,flex:1}}>
        <TouchableWithoutFeedback
        onPress={()=>Keyboard.dismiss()}
        accessible={false}
        >
           <View style={styles.center}>
           <Icon color="white" style={{marginLeft:'auto',marginRight:'auto',marginBottom:40}} size={RF(13)} name='p'/>
            <TextInput
                clearButtonMode={'while-editing'}
                keyboardAppearance={"dark"}
                returnKeyType={this.state.state == "login" ? "next" : "done"}
                style={styles.inputStyle}
                placeholder="Email"
                autoCapitalize = 'none'
                onChangeText={(text) => this.setState({username: text})}
                onSubmitEditing={() => {Keyboard.dismiss()}}
                blurOnSubmit={false}
                enablesReturnKeyAutomatically
                pointerEvents="auto"
                value={this.state.username}
            />
            {
                this.state.state != "login" ?
                <TextInput
                    clearButtonMode={'while-editing'}
                    keyboardAppearance={"dark"}
                    returnKeyType={"next"}
                    style={styles.inputStyle}
                    placeholder="First Name"
                    autoCapitalize="none"
                    onChangeText={(text) => this.setState({first_name: text})}
                    onSubmitEditing={() => {Keyboard.dismiss()}}
                    blurOnSubmit={false}
                    enablesReturnKeyAutomatically
                    pointerEvents="auto"
                    value={this.state.first_name}
                />
                : null
            }
            {
                this.state.state != "login" ?
                <TextInput
                    clearButtonMode={'while-editing'}
                    keyboardAppearance={"dark"}
                    returnKeyType={"next"}
                    style={styles.inputStyle}
                    placeholder="Last Name"
                    autoCapitalize="none"
                    onChangeText={(text) => this.setState({last_name: text})}
                    onSubmitEditing={() => {Keyboard.dismiss()}}
                    blurOnSubmit={false}
                    enablesReturnKeyAutomatically
                    pointerEvents="auto"
                    value={this.state.last_name}
                />
                : null
            }
            {
                this.state.state != "login" ?
                <TextInput
                    clearButtonMode={'while-editing'}
                    keyboardAppearance={"dark"}
                    returnKeyType={"next"}
                    style={styles.inputStyle}
                    placeholder="Password"
                    autoCapitalize="none"
                    onChangeText={(text) => this.setState({password_new: text})}
                    onSubmitEditing={() => {Keyboard.dismiss()}}
                    blurOnSubmit={false}
                    enablesReturnKeyAutomatically
                    pointerEvents="auto"
                    secureTextEntry={true}
                    value={this.state.password_new}
                />
                : null
            }
            {this.state.state == "login" ?
            <TextInput
                clearButtonMode={'while-editing'}
                keyboardAppearance={"dark"}
                style={styles.inputStyle}
                returnKeyType="done"
                placeholder="Password"
                autoCapitalize = 'none'
                secureTextEntry={true}
                onChangeText={(text) => this.setState({password: text})}
                enablesReturnKeyAutomatically
                pointerEvents="auto"
                onSubmitEditing={()=>this.requestLoginFromApi()}
                value={this.state.password}
                selectTextOnFocus={true}
                ref={(input) => { this.secondTextInput = input; }}
                /> : null }

                {
                    this.state.state == "login" ?
                    <TouchableOpacity
            style={styles.enter} 
                onPress={()=>this.requestLoginFromApi()} 
            >
                <Text style={{textAlign:'center' ,color:'#F4F8FB' ,fontSize:RF(2.8),fontFamily:'K2D-Regular',}}>Log in</Text>
            </TouchableOpacity> : 
            <TouchableOpacity
            style={styles.enter} 
                onPress={()=>this.requestSignUpFromApi()} 
            >
                <Text style={{textAlign:'center' ,color:'#F4F8FB' ,fontSize:RF(2.8),fontFamily:'K2D-Regular',}}>Sign Up</Text>
            </TouchableOpacity>
                }
                {this.state.state == "login" ?
                        <TouchableOpacity
                            activeOpacity={.8}
                            style={{padding:10,marginTop:10}}
                            onPress={()=>this.setState({state: 'signup',username:''})}
                        >
                            <Text style={{textAlign:'center' ,color:'#F4F8FB' ,fontSize:RF(2.8),fontFamily:'K2D-Regular',}}>Sign up</Text>
                        </TouchableOpacity> : 
                        <TouchableOpacity
                            activeOpacity={.8}
                            style={{padding:10,marginTop:10}}
                            onPress={()=>this.setState({state: 'login',username:''})}
                        >
                            <Text style={{textAlign:'center' ,color:'#F4F8FB' ,fontSize:RF(2.8),fontFamily:'K2D-Regular',}}>Login</Text>
                        </TouchableOpacity>   
            }


            <Text style={{textAlign:'center' ,color:'#F4F8FB' ,fontSize:RF(2.5),marginTop:20,fontFamily:'K2D-Regular'}}>{this.state.error}</Text>
        </View>
        </TouchableWithoutFeedback>
        </ImageBackground>
        </TouchableWithoutFeedback>
    );
  }

}
const styles=StyleSheet.create({
  username:{
    // backgroundColor:'#e6e6ff'
  },
   enter:{
        // flex:1,
        width:'90%',
        paddingVertical:16,
        justifyContent:'center' ,
        alignItems:'center' ,
        marginTop:10,
        marginLeft:'10%',
        marginRight:'10%',
        borderRadius:5,
        backgroundColor:'#2E4FAE'
   },
   inputStyle:{
        // backgroundColor:'#3F6BE8',
        backgroundColor:'rgba(63,107,232,1)',
        width:'90%',
        textAlign: 'center',
        paddingVertical:15,
        marginTop:10,
        borderRadius:7,
        borderColor:'#2E4FAE',
        borderWidth:1,
        fontSize:RF(3.1),
        color:'white'
   },
   center:{
        width:'100%',
        top:105,
        justifyContent:'center' ,
        alignItems:'center',
        padding: 20,
        // left:'10%',
        marginTop:20,
        // position:'absolute',
    }
})

AppRegistry.registerComponent('Login', () => Login);