console.disableYellowBox = true;
import React, { Component } from 'react'
import {Platform,StyleSheet,Text,View,Image} from 'react-native';
import CheckAuthentication from './components/Application'
import Home from './components/Home'
import ScannerPage from './components/ScannerPage';
import ScanningPage from './components/QrScan'
// import 
import {logRequests} from 'react-native-requests-logger';
import { createSwitchNavigator,createAppContainer } from 'react-navigation';
import Login from './components/Login'
import Detail from './components/Detail';
import CommentsPage from './components/CommentsPage'
import Profile from './components/Profile'
import Plan from './components/Plan'
// import { QRscanner } from 'react-native-qr-scanner';
const RootStack = createSwitchNavigator(
  {
    page1:CheckAuthentication,
    page2:ScannerPage,
    page3:Detail,
    page4:Home,
    page5:CommentsPage,
    page6:Profile,
    page7:Login,
    page8:ScanningPage,
    page9:Plan
  },
  {
    initialRouteName: 'page1',
  },
);
const AppContainer = createAppContainer(RootStack);



class All extends Component {
  constructor(props) {
    super(props);
    logRequests();
  }
  render() {
    return (
      <AppContainer/>
    )
  }
}

export default All
// export default AppContainer